# Honors Algebra 2

This repo is for tracking my notes for Honors Algebra 2 and also sharing them among classmates.

If you are looking at my notes and you have no idea what [markdown](https://en.wikipedia.org/wiki/Markdown) or [LaTeX](https://en.wikipedia.org/wiki/LaTeX) is, make sure that you look at the `.pdf` files in the `/out` folders.